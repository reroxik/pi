<?php

namespace App\Http\Controllers;


use App\Http\Cryption;
use Illuminate\Encryption\Encrypter;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class QuestionnaireController extends Controller
{
    public function available()
    {
        $user = Auth::user();
        $questionnaire = [];
        $availableQuestionnaire  = DB::select(DB::raw(
                "SELECT a.answers, q.id, q.title, q.data FROM questionnaires q
                 JOIN users u ON u.group_id = q.group_id
                 LEFT JOIN user_answers a ON q.id = a.questionnaire_id AND a.user_id = u.id where u.id =".$user->getAuthIdentifier()
            ));


        foreach ($availableQuestionnaire as $item) {
            $questionnaire[] = [
               'id' => $item->id,
              'title' => $item->title,
              'questions' => json_decode($item->data)
            ];
        }

        return view('questionnaire/list', ['questions' => $questionnaire]);
    }

    public function filled()
    {
        return view('questionnaire/filled');
    }

    public function answers(Request $request)
    {
        $user = Auth::user();
        $data['quest'] = false;
        $data['questionnaire'] = false;
        $data['isStudent'] = $user->is_student;

        if ($request->method() == "POST") {


            if (Auth::user()->is_student) {
                $res  = DB::select(DB::raw("
                SELECT
                    ua.answers as ans,
                    q.title
                FROM user_answers ua
                JOIN questionnaires q ON q.id = ua.questionnaire_id
                WHERE ua.user_id = " . $user->getAuthIdentifier() . "
                AND q.id = ". $request->input("question_id")
                ));
            $data['questionnaire']['data'] =  json_decode((new Cryption())->decryption($res[0]->ans,$request->input("hash")), true);
            } else {
                $res  = DB::select(DB::raw(
                "SELECT
                ua.answers as ans,
                q.title
                FROM questionnaire_answers ua
                JOIN questionnaires q ON q.id = ua.questionaire_id
                WHERE ua.questionaire_id = " . $request->input("question_id")
                ));
                $answers = [];
                foreach ($res as $item){
                    $itemArray = json_decode($item->ans, true);
                    foreach ($itemArray as $answer){
                        if(isset( $answers[$answer["question"]])){
                            $answers[$answer["question"]]["answer"] .= ",".$answer["answer"];
                        }else{
                            $answers[$answer["question"]] = $answer;
                        }
                    }
                   // dd(json_decode($item->ans, true));
                }
                $data['questionnaire']['data'] =  $answers;
                //dd($data);
            }

            $data['questionnaire']['ans'] = $res[0]->title;


        } else {

           if (Auth::user()->is_student) {
               $sql = "SELECT
                        q.id,
                        q.title
                        FROM questionnaires q
                        LEFT JOIN user_answers ua
                        ON ua.questionnaire_id = q.id
                        WHERE user_id = 1";
               $res = DB::select(DB::raw($sql));
           } else {
               $sql = "
                    SELECT
                    q.id,
                    q.title
                    FROM questionnaires q";

               $res = DB::select(DB::raw($sql));
        }

          $data['quest'] = $res;
        }

        return view('questionnaire/answers', $data);
    }

    public function save(Request $request)
    {
        $questionnaireId = $request->input("questionnaire");
        $encryptionKey = $request->input("hash_key");

        $answers = [];

        foreach ($request->input("answer") as $answer) {
            $answers[] = [
                "question" => key($answer),
                "answer" => current($answer)
            ];
        }

        $encryption = new Cryption();
        $user = Auth::user();


        $string = json_encode($answers);
        $str = $encryption->encryption($string, $encryptionKey);

        DB::table("user_answers")->insert([
            'user_id' => $user->getAuthIdentifier(),
            'questionnaire_id' => $questionnaireId,
            'answers' => $str,
            'created_at' => date('Y-m-d H:i:s')
        ]);

        DB::table("questionnaire_answers")->insert([
            'questionaire_id' => $questionnaireId,
            'answers' => json_encode($answers),
            'created_at' => date('Y-m-d H:i:s')
        ]);

        return redirect()->route('questionnaire.answers');
    }



}
